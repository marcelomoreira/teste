<?php

namespace App\Services\Employee;



use App\Models\Employee\Employee;

class EmployeeService
{

    private Employee $employee;

    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    /**
     * Cadastrar novo usuário
     */
    public function store(array $request)
    {
        try {
            $employeeCreate = $this->employee->create([
                'name' => $request['name'],
                'email' => $request['email'],
                'phone' => $request['phone'],
                'birth_date' => $request['birth_date'],
        ]);

        } catch (\Exception $e) {
            throw $e;
        }

        return $employeeCreate;

    }

    /**
     * Encontrar Usuário por ID
     */
    public function find(int $id)
    {
        $user = $this->employee->findOrFail($id);

        return $user;
    }

    public function update(int $id, array $request)
    {
        $employee = $this->find($id);

        $employee->name = $request['name'];
        $employee->email = $request['email'];
        $employee->phone = $request['phone'];
        $employee->birth_date = $request['birth_date'];
        $employee->save();

        return $employee;
    }
}
