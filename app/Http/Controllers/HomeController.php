<?php

namespace App\Http\Controllers;

use App\Models\Employee\Employee;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Employee $employee)
    {
        $countEmployees = $employee->count();

        return view('home', compact('countEmployees'));
    }
}
