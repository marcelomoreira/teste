<?php

namespace App\Http\Controllers\Employee;

use App\DataTables\EmployeeDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Employee\EmployeeStoreRequest;
use App\Http\Requests\Employee\EmployeeUpdateRequest;
use App\Http\Requests\Employee\StoreEmployeeRequest;
use App\Http\Requests\Employee\UpdateEmployeeRequest;
use App\Services\Employee\EmployeeService;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{

    private EmployeeService $service;

    public function __construct(EmployeeService $service)
    {
        $this->service = $service;
    }

    /**
     * Lista todos os colaboradores cadastrados
     */
    public function index(EmployeeDataTable $dataTables)
    {
        return $dataTables->render('employee.index');
    }

    /**
     * Direciona a tela de cadastro de colaborador
     */
    public function create()
    {
        return view('employee.create');
    }

    /**
     * cadastrar um novo colaborador
     */
    public function store(StoreEmployeeRequest $request)
    {
        try {
            $this->service->store($request->all());
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($e->getMessage());
        }

        toast('Cadastro Realizado com Sucesso', 'success');

        return redirect()
            ->route('employees.index');
    }

    /**
     * Direciona a tela de editar o colaborador escolhido
     */
    public function edit($id)
    {
        $employee = $this->service->find($id);

        return view('employee.edit', compact('employee'));
    }

    /**
     * Atualizar um colaborador
     */
    public function update(UpdateEmployeeRequest $request, $id)
    {
        try {
            $this->service->update($id, $request->all());
        } catch (\Exception $e) {

            return redirect()
                ->back()
                ->withInput()
                ->withErrors($e->getMessage());
        }

        toast('Colaborador Atualizado com Sucesso', 'success');

        return redirect()
            ->route('employees.index');
    }

    /**
     * Remover um colaborador
     */
    public function destroy($id)
    {
        $employee = $this->service->find($id);
        $employee->delete();

        toast('Colaborador Removido com Sucesso', 'success');

        return redirect()
            ->route('employees.index');
    }
}
