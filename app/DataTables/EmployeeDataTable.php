<?php

namespace App\DataTables;

use App\Models\Employee\Employee;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class EmployeeDataTable extends DataTable
{
    /**
     * Build DataTable class.
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at', function ($employee) {
                return \Carbon\Carbon::parse($employee->created_at)->format('d/m/Y H:i');
            })
            ->editColumn('birth_date', function ($employee) {
                return \Carbon\Carbon::parse($employee->birth_date)->format('d/m/Y');
            })
            ->addColumn('action', 'employee.actions');
    }

    /**
     * Get query source of dataTable.
     */
    public function query(Employee $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     **/
    public function html()
    {
        return $this->builder()
            ->columns([
                'name' => ['title' => 'Nome'],
                'email' => ['title' => 'E-mail'],
                'phone' => ['title' => 'Telefone'],
                'birth_date' => ['title' => 'Data de Nascimento'],
                'created_at' => ['title' => 'Cadastrado em:']
            ])
            ->minifiedAjax()
            ->language("https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese-Brasil.json")
            ->pageLength(10)
            ->lengthMenu([[10, 15, 25], [10, 15, 25]])
            ->addAction(['title' => '']);
    }

    /**
     * Get columns.
     */
    protected function getColumns()
    {
        return [
            Column::make('name'),
            Column::make('email'),
            Column::make('phone'),
            Column::make('birth_date'),
            Column::make('created_at'),
        ];
    }

    /**
     * Get filename for export.
     */
    protected function filename()
    {
        return 'Employee_' . date('YmdHis');
    }
}
