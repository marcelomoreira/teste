@extends('adminlte::page')

@section('title', 'Cadastrar Usuário - Colaborador')

@section('content_header')
    <h3>Cadastrar Usuário - Colaborador</h3>
@stop

@section('content')
    @include('sweetalert::alert')
    @include('layouts.alert')

    <div class="row">
        <div class="col-12 mb-2 d-inline-flex flex-row-reverse">
            <a href="{{ route('employees.index') }}" class="btn btn-primary btn-md text-white">Lista Colaboradore <i
                    class="fas fa-fw fa-list"></i></a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form action="{{ route('employees.store') }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">Nome</label>
                                    <input type="text"
                                           name="name"
                                           id="name"
                                           class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">E-mail</label>
                                    <input type="email"
                                           name="email"
                                           id="email"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="phone">Telefone</label>
                                    <input type="text"
                                           maxlength="13"
                                           name="phone"
                                           id="phone"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="birth_date">Data de Nascimento</label>
                                    <input type="date"
                                           name="birth_date"
                                           id="birth_date"
                                           class="form-control">
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-success">Cadastrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>

    <script>
        $('#phone').mask('## #####-####', {reverse: true});

        document.getElementById('branch').addEventListener('change', (e) => {
            const company_branch_id = document.getElementById('branch').value;
            fetch(`{{ Route::get('')->domain() }}/ajax/company-branch/${company_branch_id}/shifts`)
                .then((response) => response.json())
                .then((data) => {
                    const {response} = data;
                    document.getElementById('shift_id').length = 0;
                    response.map(({name, id}, index) => {
                        const option = document.createElement("option");
                        option.text = name;
                        option.value = id;
                        if (index === (response.length - 1)) {
                            $('shift_id').val(id);
                        }
                        document.getElementById('shift_id').add(option);
                    })
                })
        });

        //Função de somente receber número
        function onlyNumber(evt) {
            let theEvent = evt || window.event;
            let key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            //var regex = /^[0-9.,]+$/;
            let regex = /^[0-9.]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
@stop
