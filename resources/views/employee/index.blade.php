@extends('adminlte::page')

@section('title', 'Lista de Colaboradores')

@section('content_header')
    <h3>Lista de Colaboradores</h3>
@stop

@section('content')
    @include('sweetalert::alert')

    <div class="row">
        <div class="col-12 mb-2 d-inline-flex flex-row-reverse">
            <a href="{{ route('employees.create') }}" class="btn btn-primary btn-md text-white">Adicionar Novo Colaborador <i
                        class="fas fa-fw fa-plus"></i></a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    {!! $dataTable->table() !!}
                </div>
            </div>
        </div>
    </div>
@endsection


@section('css')
    <link href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css" rel="stylesheet"/>
@stop

@section('js')
    {!! $dataTable->scripts() !!}
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
@endsection



