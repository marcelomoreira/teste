{{-- Editar Usuário --}}
<a href="{{ route('employees.edit', $id) }}" class="btn btn-warning btn-sm">
    <i class='fas fa-edit'></i>
</a>

<button data-toggle="modal" data-target="#deleteModalCenter" class="btn btn-danger btn-sm" title="Remover"><i class="fas fa-trash"></i></button>

<!-- Modal Deletar Usuário -->
<div class="modal fade" id="deleteModalCenter" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Remover Colaborador</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('employees.destroy', $id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="card-body">
                        <div class="row">
                            <h5>Deseja remover esse colaborador?</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button class="btn btn-success">Confirmar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
