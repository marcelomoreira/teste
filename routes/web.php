<?php

use App\Http\Controllers\Employee\EmployeeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    /**
     * Rota Home
     */
    Route::get('/home', [HomeController::class, 'index'])->name('home');

    /**
     * Rotas CRUD Usuários Colaboradores
     */
    Route::prefix('usuarios')->group(function () {

        Route::get('/lista', [EmployeeController::class, 'index'])->name('employees.index');
        Route::get('/criar', [EmployeeController::class, 'create'])->name('employees.create');
        Route::post('/cadastrar', [EmployeeController::class, 'store'])->name('employees.store');
        Route::get('/editar/{id}', [EmployeeController::class, 'edit'])->name('employees.edit');
        Route::put('/atualizar/{id}', [EmployeeController::class, 'update'])->name('employees.update');
        Route::delete('/remover/{id}', [EmployeeController::class, 'destroy'])->name('employees.destroy');

    });

});

