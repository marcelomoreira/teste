# CRUD de Usuários

## Teste realizado para a empresa: Meeta Solutions

## O que é este projeto?
- O projeto tem como objetivo realizar um CRUD de usuários(colaboradores) no Laravel 8 utilizando Bootstrap e Datatables como ferramentas de Front-end e MySQL como o banco de dados da aplicação.

- Sistema com Autenticação de Login / Novo Registro de Usuário;
- Através de um Painel Admnistrativo, é possível Listar, Cadastrar, Editar e Remover usuários (Colaboradores);
- Na tela Home, tem um dashboard com a quantidade total de Usuários(colaboradores) cadastrados.

## Para rodar este projeto
```bash
$ git clone https://marcelomoreira@bitbucket.org/marcelomoreira/teste.git
$ cd teste
$ composer install 
$ npm install && npm run dev
$ cp .env.example .env

 ------------ Configurar Banco de Dados no Arquivo .env ------------
 
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=your_database_name
    DB_USERNAME=your_database_user
    DB_PASSWORD=your_database_password
    
 -------------------------------------------------------------------

$ php artisan key:generate
$ php artisan migrate #antes de rodar este comando verifique sua configuracao com banco em .env
$ php artisan db:seed #para gerar os seeders, dados de teste
$ php artisan serve
```
## Para Acessar Login Inicial

- Login:  admin@email.com
- Senha: 123456789
